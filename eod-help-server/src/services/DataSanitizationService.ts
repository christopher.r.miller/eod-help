import { getDatabase } from './MongoDbService';
import Timeout = NodeJS.Timeout;
import { getLogger } from './LoggingService';
import { getActiveConfiguration } from '../config/server-config';

let serviceHandle: Timeout | undefined;

const escapeRegExp = (value: string) => {
    return value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

/**
 *  Removes all expired call forwarding links from the database.
 */
const deleteExpiredForwardingLinks = async () => {
    const NOW = new Date();

    return getDatabase()
        .db('eod')
        .collection('temp_links')
        .deleteMany({ expire: { $lt: NOW } });
};

/**
 *  Removes all expired authentication tokens from the database.
 */
const deleteExpiredAuthenticationCodes = async () => {
    const NOW = new Date();

    return getDatabase()
        .db('eod')
        .collection('auth_codes')
        .deleteMany({ expire: { $lt: NOW } });
};

/**
 *  Removes all blocked users from the application.
 */
const deleteUsersOnBlocklist = async () => {
    const applicationOptions = getActiveConfiguration().options;

    if (applicationOptions.blockList) {
        for (const phoneNumber of applicationOptions.blockList) {
            let deleteResult = await getDatabase()
                .db('eod')
                .collection('registered_users')
                .deleteMany({ phoneNumber: { $regex: escapeRegExp(phoneNumber) } });

            if (deleteResult.deletedCount && deleteResult.deletedCount > 0) {
                getLogger().info(`Removed blocked user ${phoneNumber} from registered users list`);
            }

            deleteResult = await getDatabase()
                .db('eod')
                .collection('users')
                .deleteMany({ phoneNumber: { $regex: escapeRegExp(phoneNumber) } });

            if (deleteResult.deletedCount && deleteResult.deletedCount > 0) {
                getLogger().info(`Removed blocked user ${phoneNumber} from users list`);
            }
        }
    }
};

/**
 *  Main handler for this service.
 */
const onTriggerService = () => {
    Promise.all([
        deleteExpiredAuthenticationCodes(),
        deleteExpiredForwardingLinks(),
        deleteUsersOnBlocklist(),
    ]).catch((e) => {
        getLogger().error('DataSanitizationService threw an error during execution: ', e);
    });
};

/**
 *  Starts the service if it isn't already running.
 */
export const startService = () => {
    const MILLISECONDS_TO_MINUTES = 60000;
    const FIVE_MINUTES = 5 * MILLISECONDS_TO_MINUTES;

    if (!serviceHandle) {
        serviceHandle = setInterval(onTriggerService, FIVE_MINUTES);
        getLogger().info('DataSanitizationService started');
    }
};

/**
 *  Stops the service if it's currently running.
 */
export const stopService = () => {
    if (serviceHandle) {
        clearInterval(serviceHandle);
        getLogger().info('DataSanitizationService stopped');
    }
};
