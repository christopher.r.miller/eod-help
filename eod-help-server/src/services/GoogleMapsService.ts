import { Client, TravelMode, TravelRestriction } from '@googlemaps/google-maps-services-js';
import { getActiveConfiguration } from '../config/server-config';
import { IGeodeticPosition } from '../models/Location';
import { getLogger } from './LoggingService';

export const getDistanceInMinutes = async (
    center: IGeodeticPosition,
    destination: IGeodeticPosition
): Promise<number | null> => {
    // Get the distance between the two points from Google reverse geocoding API
    const client = new Client();

    try {
        const distanceMinutes = await client.distancematrix({
            params: {
                key: getActiveConfiguration().options.googleApiKey,
                avoid: [TravelRestriction.tolls],
                mode: TravelMode.driving,
                origins: [
                    {
                        lat: center.latitude,
                        lng: center.longitude,
                    },
                ],
                destinations: [
                    {
                        lat: destination.latitude,
                        lng: destination.longitude,
                    },
                ],
            },
        });

        if (distanceMinutes.status && distanceMinutes.data.rows.length === 0) {
            return null;
        } else {
            return distanceMinutes.data.rows[0].elements[0].duration.value / 60;
        }
    } catch (e) {
        getLogger().info('Error while querying reverse geolocation API: ' + e);
    }

    return null;
};
