import twilio from 'twilio';
import Timeout = NodeJS.Timeout;
import { getDatabase } from './MongoDbService';
import { getLogger } from './LoggingService';
import { getActiveConfiguration } from '../config/server-config';
import { setLongInterval } from '../utilities/Utilties';

let serviceHandle: Timeout | undefined;

const REGISTRATION_NOTIFICATION_MESSAGE = `This is a reminder that you are currently registered as a volunteer for the EOD Help Line. 
        If you are no longer interested in being included in this service, please respond with 'Remove' to be removed from the pool of volunteers. 
        Otherwise, please disregard this message.`;

/**
 *  Sends a notification to all registered users that gives them the option to
 *  opt-out of their registration.
 */
const notifyRegisteredUsers = async () => {
    const NOW = new Date();

    const allValidUsers = await getDatabase()
        .db('eod')
        .collection('users')
        .find({
            name: { $exists: true },
            phoneNumber: { $exists: true },
        })
        .toArray();

    const client = twilio(
        getActiveConfiguration().options.twilioAccountSsid,
        getActiveConfiguration().options.twilioAccountAuthKey
    );

    allValidUsers.forEach((user) => {
        client.messages.create(
            {
                body: REGISTRATION_NOTIFICATION_MESSAGE,
                from: getActiveConfiguration().options.twilioNumbers[0],
                to: user.phoneNumber,
            },
            (error) => {
                if (error) {
                    getLogger().error(
                        `Unable to send notification text to: ${user.phoneNumber}`,
                        error
                    );
                }
            }
        );
    });
};

/**
 *  Main handler for this service.
 */
const onTriggerService = () => {
    console.log('Sending user re-registration notification');
    Promise.all([notifyRegisteredUsers()]).catch((e) => {
        getLogger().error('the NotificationService threw an error during execution: ', e);
    });
};

/**
 *  Starts the service if it isn't already running.
 */
export const startService = () => {
    const MILLISECONDS_TO_MONTHS = 2.628e9;
    const THREE_MONTHS = 3 * MILLISECONDS_TO_MONTHS;

    if (!serviceHandle) {
        serviceHandle = setLongInterval(onTriggerService, THREE_MONTHS);
        getLogger().info('NotificationService started');
    }
};

/**
 *  Stops the service if it's currently running.
 */
export const stopService = () => {
    if (serviceHandle) {
        clearInterval(serviceHandle);
        getLogger().info('NotificationService stopped');
    }
};
