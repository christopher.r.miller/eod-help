import { createLogger, format, Logger, transports } from 'winston';

let _logger: Logger;

const buildLogger = () => {
    if (!_logger) {
        _logger = createLogger({
            level: 'info',
            format: format.json(),
            transports: [
                //
                // - Write all logs with level `error` and below to `error.log`
                // - Write all logs with level `info` and below to `combined.log`
                //
                new transports.File({ filename: 'error.log', level: 'error' }),
                new transports.File({ filename: 'combined.log' }),
            ],
        });

        //
        // If we're not in production then log to the `console` with the format:
        // `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
        //
        if (process.env.NODE_ENV !== 'production') {
            _logger.add(
                new transports.Console({
                    format: format.simple(),
                })
            );
        }
    }
};

buildLogger();
export const getLogger = () => _logger;
