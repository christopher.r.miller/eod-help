import { MongoClient, MongoError } from 'mongodb';
import { getActiveConfiguration } from '../config/server-config';

let _db: MongoClient;

export const connectDatabase = async (callback: (error: MongoError) => void) => {
    try {
        MongoClient.connect(
            getActiveConfiguration().options.mongoDbConnectionString,
            (error, db) => {
                _db = db;
                return callback(error);
            }
        );
    } catch (e) {
        throw e;
    }
};

export const getDatabase = () => _db;

export const disconnectDatabase = () => _db.close();
