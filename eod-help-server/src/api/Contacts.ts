import { RequestHandler, Router } from "express";
import { StatusCodes } from 'http-status-codes';
import { getActiveConfiguration } from '../config/server-config';
import { getLatLongBounds, IGeodeticPosition } from '../models/Location';
import { getDatabase } from '../services/MongoDbService';
import { getDistanceInMinutes } from '../services/GoogleMapsService';
import { decrypt, encrypt } from '../models/Security';
import phone from 'phone';
import path from 'path';
import { getLogger } from '../services/LoggingService';
import { handleOptOut } from './Notifications';
import * as Models from 'eod-help-core';

const contactsRouter: Router = Router();

interface IListRequestBody {
    clientId: string;
    location?: IGeodeticPosition;
}

interface ILinkRequestBody {
    clientId: string;
    incomingPhoneNumber: string;
}

interface IRegisterRequestBody {
    firstName: string;
    lastName: string;
    emailAddress: string;
    phoneNumber: string;
    location: IGeodeticPosition;
    additionalInformation: string;
    status: string;
}

const handleServeContactCardRequest: RequestHandler = async (req, res) => {
    return res.download(
        path.join(__dirname, '../../resources/supportLineContactCard.vcf'),
        'SupportLineContactCard.vcf'
    );
}

const handleCheckRegistrationRequest: RequestHandler = async (req, res) => {
    const clientRequest = req.body as Models.ICheckRegistrationRequest;

        if (clientRequest.phoneNumber === undefined) {
            return res.status(StatusCodes.BAD_REQUEST).send('No phone number provided');
        }

        let parsedPhoneNumbers = phone(clientRequest.phoneNumber);

        if (parsedPhoneNumbers.length < 1) {
            return res.status(StatusCodes.BAD_REQUEST).send('Invalid phone number provided.');
        }

        const phoneNumber = parsedPhoneNumbers[0];

        const applicationConfig = getActiveConfiguration().options;
        if (applicationConfig.blockList && applicationConfig.blockList.includes(phoneNumber)) {
            return res.status(StatusCodes.BAD_REQUEST).send('Service unavailable');
        }

        const existingRecord = await getDatabase()
            .db('eod')
            .collection('registered_users')
            .findOne({ phoneNumber: phoneNumber });

        if (existingRecord) {
            return res.status(StatusCodes.OK).send({
                isRegistered: true,
                isActive: existingRecord.isActive,
                phoneNumber: phoneNumber,
            });
        } else {
            return res.status(StatusCodes.OK).send({
                isRegistered: false,
                isActive: false,
                phoneNumber: phoneNumber,
            });
        }
}

const handleListNearbyContactsRequest: RequestHandler = async (req, res) => {
    const clientRequest = req.body as Models.IListNearbyRequest;

        if (clientRequest.clientId === undefined) {
            return res.status(StatusCodes.BAD_REQUEST).send('No user identifier provided');
        }

        // Validate the request body before continuing.
        if (
            clientRequest.location === undefined ||
            clientRequest.location.latitude === undefined ||
            clientRequest.location.longitude === undefined
        ) {
            // Request invalid without their current location.
            return res.status(StatusCodes.BAD_REQUEST).send('No user location provided');
        }

        try {
            let parsedPhoneNumbers = phone(clientRequest.clientId);

            if (parsedPhoneNumbers.length < 1) {
                return res.status(StatusCodes.BAD_REQUEST).send('Invalid phone number provided.');
            }

            const clientPhoneNumber = parsedPhoneNumbers[0];

            const applicationConfig = getActiveConfiguration().options;
            if (
                applicationConfig.blockList &&
                applicationConfig.blockList.includes(clientPhoneNumber)
            ) {
                return res.status(StatusCodes.OK).send([]);
            }

            getLogger().info('Finding nearby clients for ' + clientPhoneNumber);

            // Get approximate distance to top 4 nearby users
            const nearbyUserData: Models.INearbyUser[] = [];

            // Check for a matching user in the user in the database.
            const userData = await getDatabase()
                .db('eod')
                .collection('users')
                .findOne({ phoneNumber: clientPhoneNumber });

            if (userData && userData.nearbyClients) {
                // We already have clients cached for this user. Check to make sure they are all valid.
                // Use these cached clients. Make sure the clients cached here still exist in the users database. If we have stale user data, remove it.
                const nearbyClientPhoneNumbers: string[] = userData.nearbyClients.map(
                    (client: Models.INearbyUser) => client.userId
                );

                const neighbors = await getDatabase()
                    .db('eod')
                    .collection('registered_users')
                    .find({ phoneNumber: { $in: nearbyClientPhoneNumbers } })
                    .toArray();

                if (neighbors.length !== userData.nearbyClients.length) {
                    // The data in the database is out of sync!!!! ERROR!!!
                    getLogger().warn(
                        `User "${clientPhoneNumber}" needs to be reconciled in the database. This is most likely a bug in the source code.`
                    );
                }

                neighbors.forEach((neighbor) => {
                    if (neighbor.isActive) {
                        const cachedNeighbor = userData.nearbyClients.find(
                            (x: Models.INearbyUser) => x.userId == neighbor.phoneNumber
                        );

                        if (cachedNeighbor) {
                            nearbyUserData.push({
                                name: neighbor.name,
                                distanceMinutes: cachedNeighbor.distanceMinutes,
                                userId: encrypt(neighbor.phoneNumber),
                            });
                        }
                    }
                });
            }

            if (nearbyUserData.length < 4) {
                const SEARCH_RADIUS_OPTIONS: { miles: number; minutes: number }[] = [
                    {
                        miles: 100,
                        minutes: 120,
                    },
                    {
                        miles: 200,
                        minutes: 240,
                    },
                    {
                        miles: 400,
                        minutes: 460,
                    },
                    {
                        miles: 800,
                        minutes: 880,
                    },
                ];

                const KILOMETERS_PER_MILE = 1.60934;

                const searchedPhoneNumbers: string[] = [];

                for (let searchRadius of SEARCH_RADIUS_OPTIONS) {
                    if (nearbyUserData.length >= 4) {
                        // Only find the FIRST 4 nearby people.
                        break;
                    }

                    // Find all users within N miles
                    const searchRadiusBounds = getLatLongBounds(
                        clientRequest.location,
                        searchRadius.miles * KILOMETERS_PER_MILE
                    );

                    const usersInRadius = await getDatabase()
                        .db('eod')
                        .collection('registered_users')
                        .find({
                            phoneNumber: {
                                $ne: clientPhoneNumber,
                                $nin: searchedPhoneNumbers,
                            },
                            isActive: true,
                            'lastLocation.latitude': {
                                $lte: searchRadiusBounds.maximum.latitude,
                                $gte: searchRadiusBounds.minimum.latitude,
                            },
                            'lastLocation.longitude': {
                                $lte: searchRadiusBounds.maximum.longitude,
                                $gte: searchRadiusBounds.minimum.longitude,
                            },
                        })
                        .project({
                            name: 1,
                            phoneNumber: 1,
                            'lastLocation.latitude': 1,
                            'lastLocation.longitude': 1,
                        })
                        .toArray();

                    searchedPhoneNumbers.push(...usersInRadius.map((x) => x.phoneNumber.toString()));

                    for (let i = 0; i < usersInRadius.length; ++i) {
                        const distanceMinutes = await getDistanceInMinutes(clientRequest.location, {
                            latitude: usersInRadius[i].lastLocation.latitude,
                            longitude: usersInRadius[i].lastLocation.longitude,
                        });

                        if (distanceMinutes && distanceMinutes < searchRadius.minutes) {
                            nearbyUserData.push({
                                name: usersInRadius[i].name,
                                distanceMinutes: distanceMinutes,
                                userId: encrypt(usersInRadius[i].phoneNumber),
                            });
                        }

                        if (nearbyUserData.length >= 4) {
                            // Only find the FIRST 4 nearby people.
                            break;
                        }
                    }
                }
            }

            // Cache nearby user data
            await getDatabase()
                .db('eod')
                .collection('users')
                .updateOne(
                    { phoneNumber: clientPhoneNumber },
                    {
                        $set: {
                            phoneNumber: clientPhoneNumber,
                            nearbyClients: nearbyUserData.map((encryptedUserData) => {
                                return {
                                    name: encryptedUserData.name,
                                    distanceMinutes: encryptedUserData.distanceMinutes,
                                    userId: decrypt(encryptedUserData.userId),
                                };
                            }),
                        },
                    },
                    { upsert: true }
                );

            return res
                .status(StatusCodes.OK)
                .send({ nearbyUsers: nearbyUserData } as Models.IListNearbyResponse);

            // Save the closest users to the database for retrieval later.
        } catch (e) {
            getLogger().info('An error occurred while reaching out to the database:' + e);
        }

        res.status(StatusCodes.OK).send([]);
}

const handleLinkContactsRequest: RequestHandler = async (req, res) => {
    const clientRequest = req.body as Models.ICreateCallLinkRequest;
       getLogger().info('Preparing call forwarding for: ', req.body);

       // Validate the request body before continuing.
       if (clientRequest.clientId === undefined) {
           return res.status(StatusCodes.BAD_REQUEST).send('No user identifier provided');
       }

       if (clientRequest.incomingPhoneNumber === undefined) {
           return res.status(StatusCodes.BAD_REQUEST).send('No user phone number provided');
       }

       try {
           let parsedPhoneNumbers = phone(clientRequest.incomingPhoneNumber);

           if (parsedPhoneNumbers.length < 1) {
               return res.status(StatusCodes.BAD_REQUEST).send('Invalid phone number provided.');
           }

           const incomingPhoneNumber = parsedPhoneNumbers[0];

           const applicationConfig = getActiveConfiguration().options;
           if (
               applicationConfig.blockList &&
               applicationConfig.blockList.includes(incomingPhoneNumber)
           ) {
               return res.status(StatusCodes.BAD_REQUEST).send('Service unavailable.');
           }

           const expiryDate = new Date();
           expiryDate.setDate(expiryDate.getDate() + 1);

           // Check for a matching user in the user in the database.
           const insertResult = await getDatabase()
               .db('eod')
               .collection('temp_links')
               .updateOne(
                   { phoneNumber: incomingPhoneNumber },
                   {
                       $set: {
                           phoneNumber: incomingPhoneNumber,
                           clientId: decrypt(clientRequest.clientId),
                           expire: expiryDate,
                       },
                   },
                   { upsert: true }
               );

           if (insertResult.upsertedCount === 1 || insertResult.modifiedCount === 1) {
               return res.status(StatusCodes.OK).send({
                   phoneNumber: getActiveConfiguration().options.twilioNumbers[0],
               } as Models.ICreateCallLinkResponse);
           } else {
               return res.status(StatusCodes.BAD_REQUEST).send('');
           }
           // Save the closest users to the database for retrieval later.
       } catch (e) {
           getLogger().info('An error occurred while reaching out to the database:' + e);
       }

       return res.status(StatusCodes.BAD_REQUEST).send('');
}

const handleRegisterUserRequest: RequestHandler = async (req, res) => {
    const clientRequest = req.body as Models.IRegisterUserRequest;

       if (clientRequest.firstName === undefined) {
           return res.status(StatusCodes.BAD_REQUEST).send('No first name provided');
       }

       if (clientRequest.lastName === undefined) {
           return res.status(StatusCodes.BAD_REQUEST).send('No last name provided');
       }

       if (clientRequest.emailAddress === undefined) {
           return res.status(StatusCodes.BAD_REQUEST).send('No email address provided');
       }

       if (clientRequest.phoneNumber === undefined) {
           return res.status(StatusCodes.BAD_REQUEST).send('No phone number provided');
       }

       if (
           clientRequest.location === undefined ||
           clientRequest.location.latitude === undefined ||
           clientRequest.location.longitude === undefined
       ) {
           return res.status(StatusCodes.BAD_REQUEST).send('No location provided');
       }

       let parsedPhoneNumbers = phone(clientRequest.phoneNumber);

       if (parsedPhoneNumbers.length < 1) {
           return res.status(StatusCodes.BAD_REQUEST).send('Invalid phone number provided.');
       }

       const phoneNumber = parsedPhoneNumbers[0];

       const applicationConfig = getActiveConfiguration().options;
       if (applicationConfig.blockList && applicationConfig.blockList.includes(phoneNumber)) {
           return res.status(StatusCodes.BAD_REQUEST).send('Service unavailable.');
       }

       const existingRecord = await getDatabase()
           .db('eod')
           .collection('registered_users')
           .findOne({ phoneNumber: phoneNumber });

       if (existingRecord) {
           return res
               .status(StatusCodes.BAD_REQUEST)
               .send('The phone number used is already registered with an account.');
       }

       const insertResult = await getDatabase()
           .db('eod')
           .collection('registered_users')
           .insertOne({
               name: {
                   first: clientRequest.firstName,
                   last: clientRequest.lastName,
               },
               phoneNumber: phoneNumber,
               lastLocation: clientRequest.location,
               emailAddress: clientRequest.emailAddress,
               additionalInformation: clientRequest.additionalInformation,
               isActive: true,
           });

       if (insertResult.insertedCount == 1) {
           return res.status(StatusCodes.OK).send({
               isRegistered: true,
               phoneNumber: phoneNumber,
           } as Models.IRegisterUserResponse);
       }

       return res
           .status(StatusCodes.BAD_REQUEST)
           .send('An error occurred while trying to create your account');
}

const handleChangeActiveStatusRequest: RequestHandler = async (req, res) => {
    const clientRequest = req.body as Models.IChangeStatusRequest;

        if (clientRequest.phoneNumber === undefined) {
            return res.status(StatusCodes.BAD_REQUEST).send('No phone number provided');
        }

        if (clientRequest.status === undefined) {
            return res.status(StatusCodes.BAD_REQUEST).send('No status update provided');
        }

        let parsedPhoneNumbers = phone(clientRequest.phoneNumber);

        if (parsedPhoneNumbers.length < 1) {
            return res.status(StatusCodes.BAD_REQUEST).send('Invalid phone number provided.');
        }

        const phoneNumber = parsedPhoneNumbers[0];

        const applicationConfig = getActiveConfiguration().options;
        if (applicationConfig.blockList && applicationConfig.blockList.includes(phoneNumber)) {
            return res.status(StatusCodes.BAD_REQUEST).send('Service unavailable.');
        }

        // Check if account exists
        const existingRecord = await getDatabase()
            .db('eod')
            .collection('registered_users')
            .findOne({ phoneNumber: phoneNumber });

        if (existingRecord) {
            if (clientRequest.status.toLowerCase() === 'remove') {
                await handleOptOut(phoneNumber);
                return res.status(StatusCodes.OK).send({
                    isActive: false,
                    isRegistered: false,
                } as Models.ICheckRegistrationResponse);
            }

            if (clientRequest.status.toLowerCase() === 'disable') {
                const updateResult = await getDatabase()
                    .db('eod')
                    .collection('registered_users')
                    .updateOne(
                        { phoneNumber: phoneNumber },
                        {
                            $set: {
                                isActive: false,
                            },
                        }
                    );

                if (updateResult.modifiedCount === 1) {
                    return res.status(StatusCodes.OK).send({
                        isActive: false,
                        isRegistered: true,
                    } as Models.ICheckRegistrationResponse);
                } else {
                    return res.status(StatusCodes.NOT_FOUND).send('');
                }
            }

            if (clientRequest.status.toLowerCase() === 'enable') {
                const updateResult = await getDatabase()
                    .db('eod')
                    .collection('registered_users')
                    .updateOne(
                        { phoneNumber: phoneNumber },
                        {
                            $set: {
                                isActive: true,
                            },
                        }
                    );

                if (updateResult.modifiedCount === 1) {
                    return res.status(StatusCodes.OK).send({
                        isActive: true,
                        isRegistered: true,
                    } as Models.ICheckRegistrationResponse);
                } else {
                    return res.status(StatusCodes.NOT_FOUND).send('');
                }
            }
        }
}

const handleUpdateLocationRequest: RequestHandler = async (req, res) => {
    const request = req.body as Models.IUpdateUserLocationRequest;

       if (request.phoneNumber === undefined) {
           return res.status(StatusCodes.BAD_REQUEST).send('No phone number provided');
       }

       if (
           request.location === undefined ||
           request.location.latitude === undefined ||
           request.location.longitude === undefined
       ) {
           return res.status(StatusCodes.BAD_REQUEST).send('No location provided');
       }

       let parsedPhoneNumbers = phone(request.phoneNumber);

       if (parsedPhoneNumbers.length < 1) {
           return res.status(StatusCodes.BAD_REQUEST).send('Invalid phone number provided.');
       }

       const phoneNumber = parsedPhoneNumbers[0];

       const applicationConfig = getActiveConfiguration().options;
       if (applicationConfig.blockList && applicationConfig.blockList.includes(phoneNumber)) {
           return res.status(StatusCodes.BAD_REQUEST).send('Service unavailable.');
       }

       const registeredUser = await getDatabase().db('eod').collection('registered_users').findOne({
           phoneNumber: phoneNumber,
       });

       if (!registeredUser) {
           return res
               .status(StatusCodes.BAD_REQUEST)
               .send('The phone number provided is not registered');
       }

       // Update the registered user's location
       const updateResult = await getDatabase()
           .db('eod')
           .collection('registered_users')
           .updateOne(
               {
                   phoneNumber: phoneNumber,
               },
               {
                   $set: {
                       lastLocation: request.location,
                   },
               }
           );

       if (!updateResult.result.ok) {
           return res.status(StatusCodes.BAD_REQUEST).send('Failed to update user');
       }

       // Find all cached "nearbyUsers" that match this phone number and invalidate them.
       const associatedUsers = await getDatabase()
           .db('eod')
           .collection('users')
           .find({ 'nearbyClients.userId': phoneNumber })
           .toArray();

       for (let associatedUser of associatedUsers) {
           const updateResult = await getDatabase()
               .db('eod')
               .collection('users')
               .updateOne(
                   {
                       phoneNumber: associatedUser.phoneNumber,
                   },
                   {
                       $set: {
                           nearbyClients: associatedUser.nearbyClients.filter(
                               (x: any) => x.userId !== phoneNumber
                           ),
                       },
                   }
               );

           if (updateResult.modifiedCount === 0) {
               getLogger().error(
                   `Failed to invalidate stale nearbyUser [${phoneNumber}] in document [${associatedUser.phoneNumber}].`
               );
           }
       }

       return res.status(StatusCodes.OK).send({});
}

contactsRouter.get('/contactCard', handleServeContactCardRequest);
contactsRouter.post('/checkRegistration', handleCheckRegistrationRequest);
contactsRouter.post('/listNearby', handleListNearbyContactsRequest);
contactsRouter.post('/link', handleLinkContactsRequest);
contactsRouter.post('/register', handleRegisterUserRequest);
contactsRouter.post('/changeStatus', handleChangeActiveStatusRequest);
contactsRouter.post('/updateLocation', handleUpdateLocationRequest);

export default contactsRouter;
