import phone from 'phone';
import twilio from 'twilio';
import * as Models from 'eod-help-core';
import { RequestHandler, Router } from "express";
import { StatusCodes } from 'http-status-codes';
import { getActiveConfiguration } from '../config/server-config';
import { encrypt } from '../models/Security';
import { getDatabase } from '../services/MongoDbService';
import { getLogger } from '../services/LoggingService';

const authRouter: Router = Router();

/**
 * Triggers a request to Twilio to send the code specified as an SMS to a destination
 * phone number.
 * @param phoneNumber the destination phone number.
 * @param code the verification code.
 */
const sendCodeUsingSms = (phoneNumber: string, code: string) => {
    const client = twilio(
        getActiveConfiguration().options.twilioAccountSsid,
        getActiveConfiguration().options.twilioAccountAuthKey
    );

    getLogger().info('Sending SMS verification code to: ' + phoneNumber);

    client.messages.create(
        {
            body: 'Your EOD Help code is ' + code,
            from: getActiveConfiguration().options.twilioNumbers[0],
            to: phoneNumber,
        },
        (error) => {
            if (error) {
                getLogger().error('Unable to send verification text: ', error);
            }
        }
    );
};

/**
 * Converts any number into a sequence of TwiML instructions that speak the value
 * with pauses between each character.
 * @param code the verification code to speak.
 */
const verificationCodeToTwiml = (code: string): string => {
    let twiml: string[] = [];

    for (let i = 0; i < code.length; ++i) {
        twiml.push('<Pause length="1" />');
        twiml.push(`<Say>${code.charAt(i)}</Say>`);
    }

    return twiml.join('');
};

/**
 * Triggers a Twilio call the phone number specified to speak the verification code
 * out loud.
 * @param phoneNumber the phone number to call.
 * @param code the verification code to speak.
 */
const sendCodeUsingVoice = (phoneNumber: string, code: string) => {
    const client = twilio(
        getActiveConfiguration().options.twilioAccountSsid,
        getActiveConfiguration().options.twilioAccountAuthKey
    );

    getLogger().info('Sending Phone verification code to: ' + phoneNumber);

    client.calls.create(
        {
            twiml: `<Response><Say>Your verification code is:</Say>${verificationCodeToTwiml(
                code
            )}<Pause length="3" /><Say>Again, your verification code is:</Say>${verificationCodeToTwiml(
                code
            )}<Pause length="3"/></Response>`,
            from: getActiveConfiguration().options.twilioNumbers[0],
            to: phoneNumber,
        },
        (error) => {
            if (error) {
                getLogger().error('Unable to send verification call: ', error);
            }
        }
    );
};

const handleGenerateCodeRequest: RequestHandler = async (req, res) => {
    const request = req.body as Models.IGenerateCodeRequest;

      if (request.phoneNumber === undefined) {
          return res.status(StatusCodes.BAD_REQUEST).send('No phone number provided');
      }

      if (request.verificationType === undefined) {
          return res.status(StatusCodes.BAD_REQUEST).send('No verification method selected');
      }

      const e164PhoneNumber = phone(request.phoneNumber);

      if (e164PhoneNumber.length < 1) {
          return res.status(StatusCodes.BAD_REQUEST).send('Invalid phone number provided');
      }

      const phoneNumber = e164PhoneNumber[0];

      const applicationConfig = getActiveConfiguration().options;
      if (applicationConfig.blockList && applicationConfig.blockList.includes(phoneNumber)) {
          return res.status(StatusCodes.BAD_REQUEST).send('Service unavailable');
      }

      const existingCode = await getDatabase()
          .db('eod')
          .collection('auth_codes')
          .findOne({ phoneNumber: phone });

      const resendRequested = request.resendCode !== undefined && req.body.resendCode === true;

      if (existingCode && !resendRequested) {
          return res.status(200).send({
              isSent: true,
              challengeId: existingCode.challengeToken,
          } as Models.IGenerateCodeResponse);
      }

      const challengeToken = encrypt(
          Buffer.from(Math.floor(100000 + Math.random() * 900000).toString())
      );
      const verificationCode = Math.floor(100000 + Math.random() * 900000);
      const expirationDate = new Date();
      expirationDate.setTime(expirationDate.getTime() + 600000);

      const insertResult = await getDatabase()
          .db('eod')
          .collection('auth_codes')
          .updateOne(
              { phoneNumber: phoneNumber },
              {
                  $set: {
                      phoneNumber: phoneNumber,
                      challengeToken: challengeToken,
                      verificationCode: verificationCode,
                      created: new Date(),
                      expire: expirationDate,
                  },
              },
              { upsert: true }
          );

      if (insertResult.modifiedCount === 0 && insertResult.upsertedCount === 0) {
          return res.status(StatusCodes.BAD_REQUEST).send('Failed to save code to database.');
      }

      switch (req.body.verificationType) {
          case Models.VerifyType.SMS: {
              sendCodeUsingSms(phoneNumber, verificationCode.toString());
              break;
          }
          case Models.VerifyType.Voice: {
              sendCodeUsingVoice(phoneNumber, verificationCode.toString());
              break;
          }
          default: {
              return res
                  .status(StatusCodes.BAD_REQUEST)
                  .send('Validation type requested is not supported');
          }
      }

      return res.status(StatusCodes.OK).send({
          isSent: true,
          challengeId: challengeToken,
      } as Models.IGenerateCodeResponse);
}

const handleVerifyCodeRequest: RequestHandler = async (req, res) => {
    const request = req.body as Models.IVerifyCodeRequest;

      if (request.challengeId === undefined) {
          return res.status(StatusCodes.BAD_REQUEST).send('No challenge token provided.');
      }

      if (request.verificationCode === undefined) {
          return res.status(StatusCodes.BAD_REQUEST).send('No verification code provided.');
      }

      const challengeToken = await getDatabase()
          .db('eod')
          .collection('auth_codes')
          .findOne({ challengeToken: request.challengeId });

      if (challengeToken) {
          if (challengeToken.expire && new Date() > challengeToken.expire) {
              return res.status(StatusCodes.BAD_REQUEST).send('Challenge Token invalid.');
          }

          if (challengeToken.verificationCode == request.verificationCode) {
              // Remove the token as it's already used
              await getDatabase()
                  .db('eod')
                  .collection('auth_codes')
                  .deleteOne({ _id: challengeToken._id });
          }

          return res.status(StatusCodes.OK).send({
              isValid: challengeToken.verificationCode == request.verificationCode,
          } as Models.IVerifyCodeResponse);
      } else {
          return res.status(StatusCodes.BAD_REQUEST).send('Challenge Token invalid.');
      }
}

authRouter.post('/generate', handleGenerateCodeRequest);
authRouter.post('/verify', handleVerifyCodeRequest);

export default authRouter;
