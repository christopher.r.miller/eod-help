import CliTable from 'cli-table3';
import listEndpoints from 'express-list-endpoints';
import { Express, Router } from 'express';
import authRouter from './Auth';
import callsRouter from './Calls';
import contactsRouter from './Contacts';
import notificationsRouter from './Notifications';
import settingRouter from './Settings';

/**
 * Prints the currently registered express routes as a table to the standard output stream.
 * Each entry in the table shows the registered endpoint name and method.
 * @param app the express instance.
 */
export const printApiRoutes = (app: Express) => {
    let table = new CliTable({
        head: ['Path', 'Method'],
    });

    console.log('Registered API Routes: ');
    console.log('********************************************');

    const endpoints = listEndpoints(app);

    endpoints.forEach((endpoint) => {
        table.push([endpoint.path, endpoint.methods.join(', ')]);
    });

    console.log(table.toString());
};

const router = Router();

// Register all of the endpoints for this REST API
router.use('/auth', authRouter);
router.use('/calls', callsRouter);
router.use('/contacts', contactsRouter);
router.use('/notifications', notificationsRouter);
router.use('/settings', settingRouter);

export default router;
