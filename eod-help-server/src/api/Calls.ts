import { RequestHandler, Router } from "express";
import * as twilio from 'twilio';
import { getActiveConfiguration } from '../config/server-config';
import { getDatabase } from '../services/MongoDbService';
import { getLogger } from '../services/LoggingService';
import { StatusCodes } from 'http-status-codes';

const callsRouter: Router = Router();

const escapeRegExp = (value: string) => {
    return value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

const handleConnectRequest: RequestHandler = async (req, res) => {
    const fromState = req.body;

    if (fromState) {
        const response = new twilio.twiml.VoiceResponse();
        const callerNumber = escapeRegExp(fromState.From.toString());

        const applicationConfig = getActiveConfiguration().options;
        if (applicationConfig.blockList && applicationConfig.blockList.includes(callerNumber)) {
            return res.set('Content-Type', 'text/xml').send(response.toString());
        }

        response.say('Please wait while we connect your call: ');

        try {
            // Check for a matching phone number in the user in the database.
            const linkData = await getDatabase()
                .db('eod')
                .collection('temp_links')
                .findOne({ phoneNumber: { $regex: callerNumber } });

            const targetUserData = await getDatabase()
                .db('eod')
                .collection('registered_users')
                .findOne({ phoneNumber: linkData.clientId });

            if (targetUserData && linkData) {
                response.say(
                    `Connecting you to ${targetUserData.name.first} ${targetUserData.name.last}.`
                );

                getLogger().info(`Forwarding ${linkData.phoneNumber} to ${linkData.clientId}`);
                const dialOptions = response.dial(
                    {
                        callerId: fromState.To.toString(),
                    },
                    linkData.clientId
                );

                //response.say(`Connecting you to the E O D Support Line.`);
                //response.dial(getActiveConfiguration().options.mainSupportNumber);

                await getDatabase()
                    .db('eod')
                    .collection('temp_links')
                    .deleteOne({ phoneNumber: { $regex: callerNumber } });
            } else {
                getLogger().error(`Unable to find matching link number for ${callerNumber}`);
                getLogger().info(
                    `Redirecting to the EOD Support Line ${
                        getActiveConfiguration().options.mainSupportNumber
                    }`
                );

                response.say(`Connecting you to the E O D Support Line.`);
                response.dial(getActiveConfiguration().options.mainSupportNumber);
            }

            res.set('Content-Type', 'text/xml');
            return res.send(response.toString());
        } catch (e) {
            getLogger().info('An error occurred while reaching out to the database:' + e);
        } finally {
        }
    }

    const defaultResponse = new twilio.twiml.VoiceResponse();
    defaultResponse.say(
        'You have reached the E O D Help Line. Please wait while we connect your call: '
    );
    defaultResponse.dial(getActiveConfiguration().options.mainSupportNumber);

    res.set('Content-Type', 'text/xml');
    return res.send(defaultResponse.toString());
}

callsRouter.post('/connect', handleConnectRequest);

export default callsRouter;
