import { RequestHandler, Router } from "express";
import { StatusCodes } from 'http-status-codes';
import { getActiveConfiguration } from '../config/server-config';

const settingsRouter: Router = Router();

const handleGetApplicationSettingsRequest: RequestHandler = async (req, res) => {
    return res.status(StatusCodes.OK).send({
        mainPhoneNumber: getActiveConfiguration().options.mainSupportNumber,
    });
}

settingsRouter.get('/all', handleGetApplicationSettingsRequest);

export default settingsRouter;
