import { RequestHandler, Router } from "express";
import { StatusCodes } from 'http-status-codes';
import { getActiveConfiguration } from '../config/server-config';
import { getDatabase } from '../services/MongoDbService';
import { getLogger } from '../services/LoggingService';
import MessagingResponse from 'twilio/lib/twiml/MessagingResponse';
import phone from 'phone';

const notificationsRouter: Router = Router();

const TWILIO_OPT_OUT_CODES: string[] = ['stop', 'stopall', 'unsubscribe', 'cancel', 'end', 'quit'];
const UNSUBSCRIBE_CODE = 'remove';
const UNSUBSCRIBE_MESSAGE =
    'You have successfully been removed from the EOD Help Line. You can re-register on the app at any time.';

/**
 *  Handle if the user chooses to opt-out of SMS messaging services.
 *  If they choose to opt-out then we will also remove them from the database
 *  of registered users.
 */
export const handleOptOut = async (phoneNumber: string): Promise<void> => {
    const deletedCount = await getDatabase().db('eod').collection('registered_users').deleteOne({
        phoneNumber: phoneNumber,
    });

    if (deletedCount) {
        if (deletedCount.deletedCount === undefined || deletedCount.deletedCount < 1) {
            getLogger().warn(`Opt out requested by ${phoneNumber}, but no such users exists.`);
            return;
        }
    } else {
        getLogger().warn(`Opt out requested by ${phoneNumber}, but no such users exists.`);
        return;
    }

    // Find all 'nearbyUsers' that are associated with this number and remove them
    const matchingNearbyUsers = await getDatabase()
        .db('eod')
        .collection('users')
        .find({ 'nearbyClients.userId': phoneNumber })
        .toArray();

    if (matchingNearbyUsers) {
        for (const user of matchingNearbyUsers) {
            await getDatabase()
                .db('eod')
                .collection('users')
                .updateOne(
                    { phoneNumber: user.phoneNumber },
                    {
                        $set: {
                            ...user,
                            nearbyClients: user.nearbyClients.filter(
                                (nearbyUser: any) => !nearbyUser.userId.includes(phoneNumber)
                            ),
                        },
                    }
                );
        }
    }

    getLogger().info(`Opt out requested by ${phoneNumber}. User removed from system.`);
};

const handleTwilioSmsRequest: RequestHandler = async (req, res) => {
    const twiml = new MessagingResponse();
       const fromPhoneNumber = req.body.From;
       const messageContent = req.body.Body;

       if (!fromPhoneNumber || !messageContent) {
           getLogger().error(
               'Message received from Twilio without the required From and Body fields. This is most likely an unauthorized access attempt.'
           );
       }

       if (req.body.OptOutType && TWILIO_OPT_OUT_CODES.includes(req.body.OptOutType.toLowerCase())) {
           await handleOptOut(fromPhoneNumber);
           twiml.message(UNSUBSCRIBE_MESSAGE);

           res.writeHead(200, { 'Content-Type': 'text/xml' });
           return res.end(twiml.toString());
       }

       if (messageContent.toLowerCase().includes(UNSUBSCRIBE_CODE)) {
           await handleOptOut(fromPhoneNumber);
           twiml.message(UNSUBSCRIBE_MESSAGE);

           res.writeHead(200, { 'Content-Type': 'text/xml' });
           return res.end(twiml.toString());
       }

       res.writeHead(200, { 'Content-Type': 'text/xml' });
       res.end(twiml.toString());
}

notificationsRouter.post('/sms', handleTwilioSmsRequest);

export default notificationsRouter;
