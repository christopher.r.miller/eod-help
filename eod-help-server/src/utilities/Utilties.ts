import Timeout = NodeJS.Timeout;

export const setLongInterval = (callback: any, timeout: number, ...args: any): Timeout => {
    let count = 0;
    const MAX_32_BIT_SIGNED = 2147483647;
    const maxIterations = timeout / MAX_32_BIT_SIGNED;

    const onInterval = () => {
        ++count;
        if (count > maxIterations) {
            count = 0;
            callback(args);
        }
    };

    return setInterval(onInterval, Math.min(timeout, MAX_32_BIT_SIGNED));
};

export const setLongTimeout = (callback: any, timeout: number, ...args: any): Timeout => {
    let count = 0;
    let handle: Timeout;
    const MAX_32_BIT_SIGNED = 2147483647;
    const maxIterations = timeout / MAX_32_BIT_SIGNED;

    const onInterval = () => {
        ++count;
        if (count > maxIterations) {
            count = 0;
            clearInterval(handle);
            callback(args);
        }
    };

    handle = setInterval(onInterval, Math.min(timeout, MAX_32_BIT_SIGNED));
    return handle;
};
