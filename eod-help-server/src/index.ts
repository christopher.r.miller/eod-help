import bodyParser from 'body-parser';
import express from 'express';
import fs from 'fs';
import cors from 'cors';
import http from 'http';
import https from 'https';
import path from 'path';
import helmet from 'helmet';
import expressMongoSanitize from 'express-mongo-sanitize';
import { getLogger } from './services/LoggingService';
import { Configuration, getActiveConfiguration } from './config/server-config';
import api, { printApiRoutes } from './api/Api';
import { connectDatabase, disconnectDatabase } from './services/MongoDbService';
import * as dataSantizationService from './services/DataSanitizationService';
import * as notificationService from './services/NotificationService';

/**
 *  Holds the application-specific configuration for this server.
 */
let configuration: Configuration;

getLogger().info('Reading application configuration from "server-config.json"');
configuration = getActiveConfiguration();

let app: express.Express = express();

/**
 *  Defines the properties for the HTTPS server.
 */
let appServerOptions: https.ServerOptions = {};

if (configuration.options.httpPort) {
    if (configuration.options.httpsPort) {
        getLogger().warn(
            'You have provided a port for both HTTP and HTTPS. Only the HTTPS port will be used to serve this application. If you wish to enable HTTP on a custom port, add the' +
                JSON.stringify({ useRedirect: true }) +
                'key to your server-config.json'
        );
    }

    /**
     *  Sets the default port for the HTTP listening services.
     */
    app.set('port', process.env.PORT || configuration.options.httpPort || 80);
}

if (configuration.options.httpsPort) {
    // Enable https REST and web server
    if (!configuration.options.sslServerKeyPath) {
        getLogger().error(
            'Missing full path to SSL private key. HTTPS is not allowed without one.'
        );
        process.exit(2);
    }

    if (!configuration.options.sslServerCertPath) {
        getLogger().error(
            'Missing full path to SSL certificate. HTTPS is not allowed without one.'
        );
        process.exit(2);
    }

    if (!configuration.options.sslServerCaPath) {
        getLogger().error(
            'Missing full path to SSL CA certificate. HTTPS is not allowed without one.'
        );
        process.exit(2);
    }

    appServerOptions = {
        // Server SSL private key and certificate
        key: fs.readFileSync(configuration.options.sslServerKeyPath),
        cert: fs.readFileSync(configuration.options.sslServerCertPath),
        ca: fs.readFileSync(configuration.options.sslServerCaPath),
    };

    /**
     *  Sets the default port for the HTTPS listening services.
     */
    app.set('port', process.env.PORT || configuration.options.httpsPort || 443);
}

/**
 *  Provides an error logger for both the web server and REST service.
 */
app.use((err: any, req: any, res: any, next: () => void) => {
    getLogger().info(err);
    next();
});

app.use(cors());

// Set helmet security policies (see https://expressjs.com/en/advanced/best-practice-security.html#use-tls for more information)
app.use(
    helmet.contentSecurityPolicy({
        reportOnly: true,
    })
);
app.use(helmet.dnsPrefetchControl());
app.use(helmet.expectCt());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.referrerPolicy());
app.use(helmet.xssFilter());

app.use(express.static('public/app', {}));

/**
 *  Serves up the React application.
 */
app.get('/', (req, res) => {
    return res.sendFile(path.join(__dirname, '..', 'public', 'app', 'index.html'));
});

/**
 *  Assigns all API routes that can be queried by the front-end.
 */

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Connect to the database before adding the REST API routes
connectDatabase((error) => {
    if (error) {
        getLogger().error('Failed to connect to database: ', error);
    }
}).then(() => {
    // Start all background services
    dataSantizationService.startService();
    notificationService.startService();

    app.use(expressMongoSanitize());
    app.use('/api', api);

    // Enable HTTPS redirection if set in the server properties.
    if (configuration.options.useRedirection) {
        if (configuration.options.httpsPort) {
            const httpRedirectApp = express();
            const httpPort = configuration.options.httpPort ?? 80;
            const httpsPort = configuration.options.httpsPort;

            // set up a route to redirect http to https
            httpRedirectApp.get('*', (req, res) => {
                const httpsPortExtension: string = httpsPort ? `:${httpsPort}` : '';
                const redirectionAddress: string = `https://${req.hostname}${httpsPortExtension}${req.url}`;

                // Explicit redirection to the address/port combination for HTTPS. Allows local development with custom
                // port bindings.
                res.redirect(redirectionAddress);
            });

            getLogger().info(
                `Enabling HTTP => HTTPS redirection. Requests from ${httpPort} will be forwarded to ${httpsPort}.`
            );

            httpRedirectApp.listen(configuration.options.httpPort ?? 80);
        } else {
            getLogger().warn(
                'HTTPS port is not set! HTTP to HTTPS redirection will not be enabled without it!'
            );
        }
    }

    // Output the list of all API routes.
    printApiRoutes(app);

    let httpServer: http.Server | undefined = undefined;
    let httpsServer: https.Server | undefined = undefined;
    if (configuration.options.httpPort && !configuration.options.useRedirection) {
        // Creates the HTTP(s) server.
        httpServer = http.createServer(appServerOptions, app).listen(app.get('port'), () => {
            getLogger().info('Express HTTP server listening on port ' + app.get('port'));
        });
    }

    if (configuration.options.httpsPort) {
        // Creates the HTTP(s) server.
        httpsServer = https.createServer(appServerOptions, app).listen(app.get('port'), () => {
            getLogger().info('Express HTTP server listening on port ' + app.get('port'));
        });
    }

    process.on('SIGTERM', () => {
        getLogger().info('SIGTERM signal received.');
        getLogger().info('Closing http server.');

        if (httpServer) {
            httpServer.close(() => {
                getLogger().info('Http server closed.');
            });
        }

        if (httpsServer) {
            httpsServer.close(() => {
                getLogger().info('Https server closed.');
            });
        }

        getLogger().info('Stopping background services');
        dataSantizationService.stopService();
        notificationService.stopService();

        getLogger().info('Closing database connection');
        disconnectDatabase().then(() => {
            getLogger().info('Database connection closed');
            process.exit(0);
        });
    });
});
