import crypto from 'crypto';
import { getActiveConfiguration } from "../config/server-config";

const algorithm = 'aes-256-ctr';
const iv = crypto.randomBytes(16);

export const encrypt = (text: Buffer): string => {
    const cipher = crypto.createCipheriv(algorithm, getActiveConfiguration().options.encryptionKey, iv);

    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

    return encrypted.toString('hex');
};

export const decrypt = (hash: string) => {
    const decipher = crypto.createDecipheriv(algorithm, getActiveConfiguration().options.encryptionKey, iv);

    const decrypted = Buffer.concat([decipher.update(Buffer.from(hash, 'hex')), decipher.final()]);

    return decrypted.toString();
};
