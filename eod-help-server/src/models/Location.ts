export interface IGeodeticPosition {
    latitude: number;
    longitude: number;
}

export interface IGeodeticBounds {
    minimum: IGeodeticPosition;
    maximum: IGeodeticPosition;
}

export const getLatLongBounds = (center: IGeodeticPosition, radiusKm: number): IGeodeticBounds => {
    const delta = 0.009 * radiusKm;

    return {
        maximum: {
            latitude: center.latitude + delta,
            longitude: center.longitude + delta / Math.cos((center.latitude * Math.PI) / 180.0),
        },
        minimum: {
            latitude: center.latitude - delta,
            longitude: center.longitude - delta / Math.cos((center.latitude * Math.PI) / 180.0),
        },
    };
};
