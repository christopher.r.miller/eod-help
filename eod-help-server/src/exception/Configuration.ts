export class ConfigurationNotFound extends Error {
    static DEFAULT_MESSAGE : string = "Unable to find configuration file at ";

    public constructor(path: string, message?: string) {
        if(message) {
            super(`${ConfigurationNotFound.DEFAULT_MESSAGE} "${path}": ${message}`);
        } else {
            super(`${ConfigurationNotFound.DEFAULT_MESSAGE} "${path}"`);
        }
    }
}

export class ConfigurationSchemaNotFound extends Error {
    static DEFAULT_MESSAGE : string = "Unable to find schema configuration file at ";

    public constructor(path: string, message?: string) {
        if(message) {
            super(`${ConfigurationSchemaNotFound.DEFAULT_MESSAGE} "${path}": ${message}`);
        } else {
            super(`${ConfigurationSchemaNotFound.DEFAULT_MESSAGE} "${path}"`);
        }
    }
}

export class ConfigurationInvalid extends Error {
    static DEFAULT_MESSAGE : string = "Unable to validate configuration file provided. Please review the console for more information";

    public constructor(path: string) {
        super(`${ConfigurationInvalid.DEFAULT_MESSAGE} "${path}"`);
    }
}