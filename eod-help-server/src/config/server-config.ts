import { resolve } from 'path';
import { existsSync, readFileSync } from 'fs';
import { Validator } from 'jsonschema';

import {
    ConfigurationInvalid,
    ConfigurationNotFound,
    ConfigurationSchemaNotFound,
} from '../exception/Configuration';
import { getLogger } from '../services/LoggingService';

const SCHEMA_PATH = resolve(__dirname, '../../schemas/server-config.config.json');

export interface Configuration {
    /**
     *  Specifies configuration options for the server.
     *
     *  @items.minimum 1
     */
    options: IServerConfigOptions;
}

interface IServerConfigOptions {
    /**
     *  Port number to use for incoming HTTP requests.
     */
    httpPort?: number;

    /**
     *  Port number to use for incoming HTTPS requests.
     */
    httpsPort?: number;

    /**
     *  True if all HTTP requests should automatically be forwarded to port used for HTTPS.
     */
    useRedirection?: boolean;

    /**
     *  Main support number.
     */
    mainSupportNumber: string;

    /**
     *  Twilio main number or array of numbers to use.
     */
    twilioNumbers: string[];

    /**
     *  The twilio unique ID to access their service
     */
    twilioAccountSsid: string;

    /**
     *  The twilio authentication key to access their service
     */
    twilioAccountAuthKey: string;

    /**
     *  Database connection string for MongoDB.
     */
    mongoDbConnectionString: string;

    /**
     *  The API key to use for reaching out to Google's services.
     */
    googleApiKey: string;

    /**
     *  The key to use when encrypting user information. The value can be randomly
     *  generated but should ideally be 15 or more upper, lower, and special
     *  characters.
     */
    encryptionKey: string;

    /**
     *  The full path to the HTTPS SSL server certificate to use.
     */
    sslServerCertPath?: string;

    /**
     *  The full path to the HTTPS SSL server key to use.
     */
    sslServerKeyPath?: string;

    /**
     *  The full path to the HTTPS CA cert to use.
     */
    sslServerCaPath?: string;

    /**
     *  Blocklist for phone numbers. Any number added here will not be able to use the
     *  services of this application.
     */
    blockList?: string[];
}

let configuration: Configuration | null = null;

export const getActiveConfiguration = (): Configuration => {
    if (!configuration) {
        configuration = readConfiguration();
    }

    return configuration;
};

const validateConfiguration = (config: Configuration): boolean => {
    let validator = new Validator();

    // Read schema and validate the configuration provided against it.
    if (!existsSync(SCHEMA_PATH)) {
        throw new ConfigurationSchemaNotFound(SCHEMA_PATH, 'File does not exist');
    }

    let schema = JSON.parse(readFileSync(resolve(SCHEMA_PATH)).toString());

    let validationResult = validator.validate(config, schema);

    if (validationResult.valid) {
        return true;
    } else {
        getLogger().warn('Configuration provided to the server has the following errors: ');
        validationResult.errors.forEach((value) => {
            getLogger().error(value);
        });
    }

    return false;
};

export const readConfiguration = (path?: string): Configuration => {
    let resolvedPath = resolve(path ?? './server-config.json');

    if (!existsSync(resolvedPath)) {
        throw new ConfigurationNotFound(resolvedPath, 'File does not exist');
    }

    let configurationFile: Configuration = JSON.parse(readFileSync(resolvedPath).toString());

    if (!validateConfiguration(configurationFile)) {
        throw new ConfigurationInvalid(resolvedPath);
    }

    return configurationFile;
};
