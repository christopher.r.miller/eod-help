/**
 *  Defines a simple structure for storing an errorMessage or errorCode generated
 *  by the server.
 */
export interface IResponseError {
    /**
     *  The application error code.
     */
    errorCode?: number;

    /**
     *  The application error message.
     */
    errorMessage?: string;
}

/**
 *  Represents a spherical coordinate.
 */
export interface IGeodeticPosition {
    /**
     *  The number of degrees north or south of the equator.
     */
    latitude: number;

    /**
     *  The number of degrees east or west of the equator.
     */
    longitude: number;
}

/**
 *  Represents a rectangular area bounding a range of geodetic coordinates.
 */
export interface IGeodeticBounds {
    /**
     *  The minimum coordinates.
     */
    minimum: IGeodeticPosition;

    /**
     *  The maximum coordinates.
     */
    maximum: IGeodeticPosition;
}
