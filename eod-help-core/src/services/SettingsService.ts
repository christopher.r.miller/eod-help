import { IResponseError } from './Common';

/**
 *  The request parameters for obtaining the application settings.
 */
export interface ISettingsRequest {}

/**
 *  Defines the global settings for this application.
 */
export interface IApplicationSettings {
    /**
     *  The main hotline number that displays on every page.
     */
    mainPhoneNumber: string;
}

/**
 * The response object returned containing the application settings.
 */
export interface ISettingsResponse extends IResponseError, IApplicationSettings {}

/**
 * Obtains all of the current application settings.
 * @param req the request parameters to use.
 * @return ISettingsRequest the result of the operation including the current
 * application settings.
 */
export const sendGetAllSettingsRequest = async (
    req: ISettingsRequest
): Promise<ISettingsResponse> => {
    try {
        const response = await fetch('/api/settings/all');

        if (response.ok) {
            return (await response.json()) as ISettingsResponse;
        } else {
            const errorMessage = await response.text();

            return {
                mainPhoneNumber: '',
                errorCode: response.status,
                errorMessage: errorMessage,
            };
        }
    } catch (e) {
        return {
            mainPhoneNumber: '',
            errorCode: 500,
            errorMessage: e.toString(),
        };
    }
};
