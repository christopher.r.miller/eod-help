import { IGeodeticPosition, IResponseError } from './Common';

/**
 *  The request parameters for checking the registration status of a user.
 */
export interface ICheckRegistrationRequest {
    /**
     *  The phone number to use when checking for a registered account.
     */
    phoneNumber: string;
}

/**
 *  The response object returned after checking a user's registration.
 */
export interface ICheckRegistrationResponse extends IResponseError {
    /**
     *  True if a registered user account exists; false if otherwise.
     */
    isRegistered: boolean;

    /**
     *  True if the registered user is active; false if otherwise.
     */
    isActive: boolean;

    /**
     *  The E.194 formatted phone number that matches the request.
     */
    phoneNumber: string;
}

/**
 *  The request parameters for finding all users near a specific client.
 */
export interface IListNearbyRequest {
    /**
     *  The phone number to use when checking for nearby clients.
     */
    clientId: string;

    /**
     *  The location of the user.
     */
    location?: IGeodeticPosition;
}

/**
 *  The digest of information for each nearby user.
 */
export interface INearbyUser {
    /**
     *  The number of minutes to this user.
     */
    distanceMinutes: number;

    /**
     *  The name of the nearby user.
     */
    name: string;

    /**
     *  The ephemeral Id of the nearby user.
     */
    userId: string;
}

/**
 *  The response object returned after gathering all users near a client.
 */
export interface IListNearbyResponse extends IResponseError {
    /**
     *  An array of nearby users.
     */
    nearbyUsers: INearbyUser[];
}

/**
 *  The request parameters for creating a incoming call link.
 */
export interface ICreateCallLinkRequest {
    /**
     *  The unique (encrypted) phone number of the user to forward
     *  the incoming call to.
     */
    clientId: string;

    /**
     *  The phone number of the user.
     */
    incomingPhoneNumber: string;
}

/**
 *  The response object returned including the phone number for the
 *  user to call to create the link.
 */
export interface ICreateCallLinkResponse extends IResponseError {
    /**
     *  The phone number for the user to call to initiate the forwarding process.
     */
    phoneNumber: string;
}

/**
 *  The request parameters for registering a user.
 */
export interface IRegisterUserRequest {
    /**
     *  The user's first name.
     */
    firstName: string;

    /**
     *  The user's last name.
     */
    lastName: string;

    /**
     *  The user's email address.
     */
    emailAddress: string;

    /**
     *  The user's verified phone number.
     */
    phoneNumber: string;

    /**
     *  The user's location.
     */
    location: IGeodeticPosition;

    /**
     *  The user's additional information.
     */
    additionalInformation: string;
}

/**
 *  The response object returned including the registration status and phone number
 *  registered.
 */
export interface IRegisterUserResponse extends IResponseError {
    /**
     *  True if the user is registered; otherwise, false.
     */
    isRegistered: boolean;

    /**
     *  The user's phone number.
     */
    phoneNumber: string;
}

/**
 *  The request parameters for changing a user's registration status.
 */
export interface IChangeStatusRequest {
    /**
     *  The user's phone number.
     */
    phoneNumber: string;

    /**
     *  The status flag to change the user's registration to.
     */
    status: 'remove' | 'disable' | 'enable';
}

/**
 *  The response object returned including the registration status and active flag.
 */
export interface IChangeStatusResponse extends IResponseError {
    /**
     *  True if the user can be contacted; false if otherwise.
     */
    isActive: boolean;

    /**
     *  True if the user is registered; otherwise, false.
     */
    isRegistered: boolean;
}

/**
 *  The request parameters for changing a user's registered location.
 */
export interface IUpdateUserLocationRequest {
    /**
     *  The user's phone number.
     */
    phoneNumber: string;

    /**
     *  The user's location.
     */
    location: IGeodeticPosition;
}

/**
 *  The response object returned including the registration status and active flag.
 */
export interface IUpdateUserLocationResponse extends IResponseError {}

/**
 * Checks the registration status for a specific user.
 * @param request the request parameters to use.
 * @return ICheckRegistrationResponse the result of the operation including the
 * registration status.
 */
export const sendCheckRegistrationRequest = async (
    request: ICheckRegistrationRequest
): Promise<ICheckRegistrationResponse> => {
    try {
        const response = await fetch('/api/contacts/checkRegistration', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request),
        });

        if (response.ok) {
            return (await response.json()) as ICheckRegistrationResponse;
        } else {
            const errorMessage = await response.text();

            return {
                isActive: false,
                isRegistered: false,
                phoneNumber: request.phoneNumber,
                errorCode: response.status,
                errorMessage: errorMessage,
            };
        }
    } catch (e) {
        return {
            isActive: false,
            isRegistered: false,
            phoneNumber: request.phoneNumber,
            errorCode: 500,
            errorMessage: e.toString(),
        };
    }
};

/**
 * Retrieves an array of all users within a 60 minute driving distance.
 * @param request the request parameters to use.
 *
 * @return IListNearbyResponse the result of the operation including an array
 * of nearby users.
 */
export const sendNearbyUserRequest = async (
    request: IListNearbyRequest
): Promise<IListNearbyResponse> => {
    try {
        const response = await fetch('/api/contacts/listNearby', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request),
        });

        if (response.ok) {
            return (await response.json()) as IListNearbyResponse;
        } else {
            const errorMessage = await response.text();

            return {
                nearbyUsers: [],
                errorCode: response.status,
                errorMessage: errorMessage,
            };
        }
    } catch (e) {
        return {
            nearbyUsers: [],
            errorCode: 500,
            errorMessage: e.toString(),
        };
    }
};

/**
 * Creates a temporary call link that will be used to forward an incoming call.
 * @param request the request parameters to use.
 *
 * @return ICreateCallLinkResponse the result of the operation including the
 * phone number the user should call to initiate the forwarding process.
 */
export const sendCallLinkRequest = async (
    request: ICreateCallLinkRequest
): Promise<ICreateCallLinkResponse> => {
    try {
        const response = await fetch('/api/contacts/link', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request),
        });

        if (response.ok) {
            return (await response.json()) as ICreateCallLinkResponse;
        } else {
            const errorMessage = await response.text();

            return {
                phoneNumber: '',
                errorCode: response.status,
                errorMessage: errorMessage,
            };
        }
    } catch (e) {
        return {
            phoneNumber: '',
            errorCode: 500,
            errorMessage: e.toString(),
        };
    }
};

/**
 * Creates a request to register a user.
 * @param request the request parameters to use.
 *
 * @return IRegisterUserResponse the result of the operation including the status
 * of the registration status and their phone number.
 */
export const sendRegistrationRequest = async (
    request: IRegisterUserRequest
): Promise<IRegisterUserResponse> => {
    try {
        const response = await fetch('/api/contacts/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request),
        });

        if (response.ok) {
            return (await response.json()) as IRegisterUserResponse;
        } else {
            const errorMessage = await response.text();

            return {
                isRegistered: false,
                phoneNumber: '',
                errorCode: response.status,
                errorMessage: errorMessage,
            };
        }
    } catch (e) {
        return {
            isRegistered: false,
            phoneNumber: '',
            errorCode: 500,
            errorMessage: e.toString(),
        };
    }
};

/**
 * Creates a request to change the registration status of a user.
 * @param request the request parameters to use.
 *
 * @return IChangeStatusResponse the result of the operation including the status
 * of the registration status and if they're still active.
 */
export const sendChangeStatusRequest = async (
    request: IChangeStatusRequest
): Promise<IChangeStatusResponse> => {
    try {
        const response = await fetch('/api/contacts/changeStatus', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request),
        });

        if (response.ok) {
            return (await response.json()) as IChangeStatusResponse;
        } else {
            const errorMessage = await response.text();

            return {
                isActive: false,
                isRegistered: false,
                errorCode: response.status,
                errorMessage: errorMessage,
            };
        }
    } catch (e) {
        return {
            isActive: false,
            isRegistered: false,
            errorCode: 500,
            errorMessage: e.toString(),
        };
    }
};

export const sendUpdateUserLocationRequest = async (
    request: IUpdateUserLocationRequest
): Promise<IUpdateUserLocationResponse> => {
    try {
        const response = await fetch('/api/contacts/updateLocation', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request),
        });

        if (response.ok) {
            return (await response.json()) as IUpdateUserLocationResponse;
        } else {
            const errorMessage = await response.text();

            return {
                errorCode: response.status,
                errorMessage: errorMessage,
            };
        }
    } catch (e) {
        return {
            errorCode: 500,
            errorMessage: e.toString(),
        };
    }
};
