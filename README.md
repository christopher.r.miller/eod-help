# EOD Anonymous Call Forwarding Application
("EOD Help")

This application was developed originally to serve the Explosive Ordnance Disposal (EOD) community as a tool to allow users to reach out to volunteers anonymously for emotional support during times of hardship.
The project is composed of three major pieces, a React PWA that provides the user interface across mobile and desktop, a
REST service that handles requests from the client, and a common package `eod-help-core` that contains
models and methods that both the frontend and backend share for messages between the two.

### Planned Features/Upgrades:
- Generalization of application to support other branches of service (Police, Firefighters, other branches of armed services)
- Inclusion of administrative page to support:
  - Monitoring of expenses related to application usage
  - Management of users (including registered volunteers)
  - Management of users on block list
  - Management of iconography and branding
  - Management of active hours for call center and fallback phone numbers.

## Building the application
To generate an optimized SPA for deployment to a web server, use `npm run build` to start a production build. By default, the application will be copied to the `eod-help-server/public/app` folder. 

## Debugging the application
To run this application alongside a local instance of the REST service, run both with `npm run start`. The REST service
will attempt to bind to port 4000 and all requests from the `eod-help-client` will be proxied to that port.
